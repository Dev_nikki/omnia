import './index.scss';

import './components/header';
import './components/content';

import 'materialize-css/dist/css/materialize.css';
import 'materialize-css/dist/js/materialize.js';

import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';
import 'slick-carousel/slick/slick.js';

import 'dotdotdot';

const name = 'webpack2-pug-scss-boilerplate';
